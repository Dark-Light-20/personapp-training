package com.example.personapptraining;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.personapptraining.fragments.CreateMyRoutineFragment;
import com.example.personapptraining.recyclerView.MyRoutinesAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MyRoutinesListFragment extends Fragment {
    MyRoutinesAdapter myRoutinesAdapter;

    MyRoutinesListFragment(FragmentManager fragmentManager) {
        this.myRoutinesAdapter = new MyRoutinesAdapter(fragmentManager);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_layout_myroutines_list, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.myRoutines_recycler_view);
        recyclerView.setAdapter(myRoutinesAdapter);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);

        FloatingActionButton floatingActionButton = view.findViewById(R.id.btn_add_myRoutine);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateMyRoutineFragment createMyRoutineFragment = new CreateMyRoutineFragment();
                assert getFragmentManager() != null;
                getFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        createMyRoutineFragment, "create-myRoutine").commit();
                MainActivity activity = ((MainActivity)getActivity());
                assert activity != null;
                activity.setCurrentFragment("create-myRoutine");
                activity.getSupportActionBar().setTitle(R.string.create);
            }
        });

        return  view;
    }
}
