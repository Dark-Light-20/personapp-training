package com.example.personapptraining;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.personapptraining.database.DataBaseAccess;
import com.example.personapptraining.fragments.AboutFragment;
import com.example.personapptraining.fragments.ExerciseControlFragment;
import com.example.personapptraining.fragments.MainFragment;
import com.example.personapptraining.fragments.WeightControlFragment;
import com.example.personapptraining.models.Advice;
import com.example.personapptraining.models.Category;
import com.example.personapptraining.models.Exercise;
import com.example.personapptraining.models.MyRoutine;
import com.example.personapptraining.models.Record;
import com.example.personapptraining.models.Routine;
import com.google.android.material.navigation.NavigationView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    private String currentFragment;
    private ExerciseListFragment exerciseListFragment;
    private CategoriesListFragment categoriesListFragment;
    private RoutinesListFragment routinesListFragment;
    private MyRoutinesListFragment myRoutinesListFragment;

    private ArrayList<Exercise> exercises;
    private ArrayList<Routine> routines;
    private ArrayList<MyRoutine> myRoutines;
    private ArrayList<Category> categories;

    private ArrayList<ArrayList<String>> exercisesSources = new ArrayList<>();
    private ArrayList<ArrayList<String>> categoriesSources = new ArrayList<>();
    private ArrayList<ArrayList<String>> routinesSources = new ArrayList<>();

    private ArrayList<Record> exerciseRecords = new ArrayList<>();
    private Record currentExRecord;

    private ArrayList<Record> weightRecords = new ArrayList<>();
    private Record currentWgRecord;
    private String idealWeight;

    private ArrayList<Advice> advices = new ArrayList<>();
    private AdvicesListFragment advicesListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Init Lists
        exerciseListFragment = new ExerciseListFragment(getSupportFragmentManager());
        categoriesListFragment = new CategoriesListFragment(getSupportFragmentManager());
        routinesListFragment = new RoutinesListFragment(getSupportFragmentManager());
        myRoutinesListFragment = new MyRoutinesListFragment(getSupportFragmentManager());
        advicesListFragment = new AdvicesListFragment(getSupportFragmentManager());

        // NavigationDrawer Menu
        Toolbar toolbar = findViewById(R.id.toolbar);
         setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        //

        // Load Data

        /// Exercises
        loadExercises();
        exerciseListFragment.exercisesAdapter.addExercises(exercises);

        // Routines
        loadRoutines();
        routinesListFragment.routinesAdapter.addRoutines(routines);

        // My Routines
        loadMyRoutines();
        myRoutinesListFragment.myRoutinesAdapter.addMyRoutines(myRoutines);

        // Categories
        loadCategories();
        categoriesListFragment.categoriesAdapter.addCategories(categories);

        // Sources
        loadSources();

        // Exercise Count Records
        loadExRecords();

        // Weight Records
        loadWeightRecords();

        // Advices
        loadAdvices();
        advicesListFragment.advicesAdapter.addAdvices(advices);

        // Main Fragment
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new MainFragment(),
                "main-fragment").commit();
        currentFragment="main-fragment";
    }

    private void loadExercises() {
        exercises = new ArrayList<>();

        DataBaseAccess dataBaseAccess = DataBaseAccess.getInstance(getApplicationContext());
        dataBaseAccess.open();

        exercises = dataBaseAccess.getDbExercises();

        dataBaseAccess.close();
    }

    private void loadRoutines() {
        routines = new ArrayList<>();

        DataBaseAccess dataBaseAccess = DataBaseAccess.getInstance(getApplicationContext());
        dataBaseAccess.open();

        routines = dataBaseAccess.getDbRoutines(exercises);

        dataBaseAccess.close();
    }

    private void loadMyRoutines() {
        myRoutines = new ArrayList<>();

        DataBaseAccess dataBaseAccess = DataBaseAccess.getInstance(getApplicationContext());
        dataBaseAccess.open();

        myRoutines = dataBaseAccess.getDbMyRoutines(exercises);

        dataBaseAccess.close();
    }

    private void loadCategories() {
        categories = new ArrayList<>();

        DataBaseAccess dataBaseAccess = DataBaseAccess.getInstance(getApplicationContext());
        dataBaseAccess.open();

        categories = dataBaseAccess.getDbCategories();

        dataBaseAccess.close();
    }

    private void loadSources() {
        BufferedReader bufferedReader = null;

        // Exercises
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(getAssets().open("exercises-sources.txt")));
            String line;
            while ((line = bufferedReader.readLine())!=null) {
                String id, source;
                id = line.substring(0,4);
                source = line.substring(5);
                ArrayList<String> sourceLine = new ArrayList<>();
                sourceLine.add(id);
                sourceLine.add(source);
                exercisesSources.add(sourceLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        // Categories
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(getAssets().open("categories-sources.txt")));
            String line;
            while ((line = bufferedReader.readLine())!=null) {
                String id, source;
                id = line.substring(0,3);
                source = line.substring(4);
                ArrayList<String> sourceLine = new ArrayList<>();
                sourceLine.add(id);
                sourceLine.add(source);
                categoriesSources.add(sourceLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        // Routines
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(getAssets().open("routines-sources.txt")));
            String line;
            while ((line = bufferedReader.readLine())!=null) {
                String id, source;
                id = line.substring(0,3);
                source = line.substring(4);
                ArrayList<String> sourceLine = new ArrayList<>();
                sourceLine.add(id);
                sourceLine.add(source);
                routinesSources.add(sourceLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void loadExRecords() {
        DataBaseAccess dataBaseAccess = DataBaseAccess.getInstance(getApplicationContext());
        dataBaseAccess.open();

        dataBaseAccess.getDbExRecords(exerciseRecords);

        String currentDate = DateFormat.getDateInstance().format(new Date());

        if (!exerciseRecords.isEmpty()) {
            Record record = exerciseRecords.get(0);
            if (currentDate.equals(record.getDate()))   currentExRecord = record;
            else {
                currentExRecord = new Record(currentDate, "0");
                ArrayList<Record> newRecords = new ArrayList<>();
                newRecords.add(currentExRecord);
                newRecords.addAll(exerciseRecords);
                exerciseRecords = newRecords;
                dataBaseAccess.addDbExRecord(currentExRecord);
            }
        }
        else {
            currentExRecord = new Record(currentDate, "0");
            ArrayList<Record> newRecords = new ArrayList<>();
            newRecords.add(currentExRecord);
            newRecords.addAll(exerciseRecords);
            exerciseRecords = newRecords;
            dataBaseAccess.addDbExRecord(currentExRecord);
        }

        dataBaseAccess.close();
    }

    public void addCurrentEx() {
        int number = Integer.parseInt(getCurrentExRecord().getValue());
        number++;
        getCurrentExRecord().setValue(String.valueOf(number));

        DataBaseAccess dataBaseAccess = DataBaseAccess.getInstance(getApplicationContext());
        dataBaseAccess.open();

        dataBaseAccess.addToCurrentExRecord();

        dataBaseAccess.close();
    }

    private void loadWeightRecords() {
        DataBaseAccess dataBaseAccess = DataBaseAccess.getInstance(getApplicationContext());
        dataBaseAccess.open();

        dataBaseAccess.getDbWgRecords(weightRecords);
        idealWeight = dataBaseAccess.getLastIdealWgRecord();

        dataBaseAccess.close();

        if (!weightRecords.isEmpty())   currentWgRecord = weightRecords.get(0);
    }

    public void addWeightRecord(Record newRecord, String ideal) {
        currentWgRecord = newRecord;
        ArrayList<Record> newRecords = new ArrayList<>();
        newRecords.add(currentWgRecord);
        newRecords.addAll(weightRecords);
        weightRecords = newRecords;
        idealWeight = ideal;

        DataBaseAccess dataBaseAccess = DataBaseAccess.getInstance(getApplicationContext());
        dataBaseAccess.open();

        dataBaseAccess.addDbWgRecord(currentWgRecord, idealWeight);

        dataBaseAccess.close();
    }

    private void loadAdvices() {
        DataBaseAccess dataBaseAccess = DataBaseAccess.getInstance(getApplicationContext());
        dataBaseAccess.open();

        dataBaseAccess.getDbAdvices(advices);

        dataBaseAccess.close();
    }

    public Exercise getExerciseByName(String name) {
        for (int i=0;i<exercises.size();i++) {
            if (name.equals(exercises.get(i).getName()))    return exercises.get(i);
        }
        return null;
    }

    public void addToMyRoutines(MyRoutine myRoutine) {
        this.myRoutines.add(myRoutine);
        myRoutinesListFragment.myRoutinesAdapter.addMyRoutines(myRoutines);

        DataBaseAccess dataBaseAccess = DataBaseAccess.getInstance(getApplicationContext());
        dataBaseAccess.open();

        dataBaseAccess.insertToMyRoutines(myRoutine, myRoutines.size());

        dataBaseAccess.close();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            return;
        }

        switch (getCurrentFragment()) {
            case "main-fragment":
                super.onBackPressed();
                break;
            case "exercise-by-list":
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        exerciseListFragment, "exercise-list-by-main").commit();
                setCurrentFragment("exercise-list-by-main");
                getSupportActionBar().setTitle(R.string.exercise_list);
                break;
            case "exercise-by-category":
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        categoriesListFragment.categoriesAdapter.getCategoryFragment(), "category-by-list").commit();
                setCurrentFragment("category-by-list");
                getSupportActionBar().setTitle(categoriesListFragment.categoriesAdapter.getCategoryFragment().getCategory().getName());
                break;
            case "category-by-list":
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        categoriesListFragment, "categories-list-by-main").commit();
                setCurrentFragment("categories-list-by-main");
                getSupportActionBar().setTitle(R.string.categories_list);
                break;
            case "exercise-by-routine":
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        routinesListFragment.routinesAdapter.getRoutineFragment(), "routine-by-list").commit();
                setCurrentFragment("routine-by-list");
                getSupportActionBar().setTitle(routinesListFragment.routinesAdapter.getRoutineFragment().getRoutine().getName());
                break;
            case "routine-by-list":
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        routinesListFragment, "routines-list-by-main").commit();
                setCurrentFragment("routines-list-by-main");
                getSupportActionBar().setTitle(R.string.routines_list);
                break;
            case "exercise-by-myRoutine":
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        myRoutinesListFragment.myRoutinesAdapter.getMyRoutineFragment(), "myRoutine-by-list").commit();
                setCurrentFragment("myRoutine-by-list");
                getSupportActionBar().setTitle(myRoutinesListFragment.myRoutinesAdapter.getMyRoutineFragment().getMyRoutine().getName());
                break;
            case "myRoutine-by-list":
            case "create-myRoutine":
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        myRoutinesListFragment, "myRoutines-list-by-main").commit();
                setCurrentFragment("myRoutines-list-by-main");
                getSupportActionBar().setTitle(R.string.my_routines);
                break;
            case "advice-by-list":
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        advicesListFragment, "advices-list-by-main").commit();
                setCurrentFragment("advices-list-by-main");
                getSupportActionBar().setTitle(R.string.advices);
                break;
            default:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new MainFragment(), "main-fragment").commit();
                setCurrentFragment("main-fragment");
                getSupportActionBar().setTitle(R.string.app_name);
                break;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.exercises_item:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        exerciseListFragment, "exercise-list-by-main").commit();
                getSupportActionBar().setTitle(R.string.exercise_list);
                currentFragment = "exercise-list-by-main";
                break;
            case R.id.categories_item:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        categoriesListFragment, "categories-list-by-main").commit();
                getSupportActionBar().setTitle(R.string.categories_list);
                currentFragment = "categories-list-by-main";
                break;
            case R.id.rutines_item:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        routinesListFragment, "routines-list-by-main").commit();
                getSupportActionBar().setTitle(R.string.routines_list);
                currentFragment = "routines-list-by-main";
                break;
            case R.id.myroutines_item:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        myRoutinesListFragment, "myRoutines-list-by-main").commit();
                getSupportActionBar().setTitle(R.string.my_routines);
                currentFragment = "myRoutines-list-by-main";
                break;
            case R.id.exercises_control_item:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new ExerciseControlFragment(exerciseRecords), "exercises-control-by-main").commit();
                getSupportActionBar().setTitle(R.string.exercises_control);
                currentFragment = "exercises-control-by-main";
                break;
            case R.id.weight_control_item:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new WeightControlFragment(weightRecords), "weight-control-by-main").commit();
                getSupportActionBar().setTitle(R.string.weight_control);
                currentFragment = "weight-control-by-main";
                break;
            case R.id.advices_item:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        advicesListFragment, "advices-list-by-main").commit();
                getSupportActionBar().setTitle(R.string.advices);
                currentFragment = "advices-list-by-main";
                break;
            case R.id.about_item:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new AboutFragment(), "about-by-main").commit();
                getSupportActionBar().setTitle(R.string.about);
                currentFragment = "about-by-main";
                break;
        }

        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        drawerLayout.closeDrawer(GravityCompat.START);

        return true;
    }

    public String getCurrentFragment() {
        return currentFragment;
    }

    public void setCurrentFragment(String currentFragment) {
        this.currentFragment = currentFragment;
    }

    public ArrayList<Exercise> getExercises() {
        return exercises;
    }

    public Record getCurrentExRecord() {
        return currentExRecord;
    }

    public Record getCurrentWgRecord() {
        return currentWgRecord;
    }

    public String getIdealWeight() {
        return idealWeight;
    }

    public void setIdealWeight(String idealWeight) {
        this.idealWeight = idealWeight;
    }

    public String getImageSource(String id) {
        ArrayList<ArrayList<String>> sources = null;
        switch (id.charAt(0)) {
            case 'e':
                sources = exercisesSources;
                break;
            case 'c':
                sources = categoriesSources;
                break;
            case 'r':
                sources = routinesSources;
                break;
        }
        id = id.substring(1);
        assert sources != null;
        for(int i = 0; i<sources.size(); i++)   if (id.equals(sources.get(i).get(0)))   return "Tomado de: "+sources.get(i).get(1);
        return "Tomado de: "+id;
    }
}

