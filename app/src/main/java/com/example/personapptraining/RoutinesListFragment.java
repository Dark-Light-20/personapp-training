package com.example.personapptraining;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.personapptraining.recyclerView.RoutinesAdapter;

public class RoutinesListFragment extends Fragment {
    RoutinesAdapter routinesAdapter;

    RoutinesListFragment(FragmentManager fragmentManager) {
        this.routinesAdapter = new RoutinesAdapter(fragmentManager);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_layout_exercise_list, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.exercises_recycler_view);
        recyclerView.setAdapter(routinesAdapter);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);

        return  view;
    }
}
