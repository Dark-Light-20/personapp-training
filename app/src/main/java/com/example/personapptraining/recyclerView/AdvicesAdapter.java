package com.example.personapptraining.recyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.personapptraining.MainActivity;
import com.example.personapptraining.R;
import com.example.personapptraining.fragments.AdviceFragment;
import com.example.personapptraining.models.Advice;

import java.util.ArrayList;

public class AdvicesAdapter extends RecyclerView.Adapter<AdvicesHolder> {
    private ArrayList<Advice> advices;
    private AdviceFragment adviceFragment;
    private FragmentManager fragmentManager;

    public AdvicesAdapter(FragmentManager fragmentManager) {
        this.advices = new ArrayList<>();
        this.fragmentManager = fragmentManager;
    }

    @NonNull
    @Override
    public AdvicesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_advice, parent, false);
        final AdvicesHolder advicesHolder = new AdvicesHolder(view);

        advicesHolder.getCardView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity activity = (MainActivity)(fragmentManager.findFragmentByTag("advices-list-by-main")).getActivity();

                adviceFragment = new AdviceFragment(advices.get(advicesHolder.getAdapterPosition()));

                fragmentManager.beginTransaction().replace(R.id.fragment_container, adviceFragment,
                        "advice-by-list").commit();

                assert activity != null;
                activity.setCurrentFragment("advice-by-list");
                activity.getSupportActionBar().setTitle(advices.get(advicesHolder.getAdapterPosition()).getName());
            }
        });

        return advicesHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AdvicesHolder holder, int position) {
        holder.getName().setText(advices.get(position).getName());
        Glide.with(holder.itemView.getContext()).load(R.drawable.health).into(holder.getImage());
    }

    @Override
    public int getItemCount() {
        return advices.size();
    }

    public void addAdvices(ArrayList<Advice> advices) {
        this.advices = advices;
        notifyDataSetChanged();
    }
}
