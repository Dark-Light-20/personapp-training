package com.example.personapptraining.recyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.personapptraining.R;

public class MyRoutinesHolder extends RecyclerView.ViewHolder {
    private ImageView image;
    private TextView name, desc_short, numberEx;
    private CardView cardView;

    MyRoutinesHolder(@NonNull View itemView) {
        super(itemView);

        cardView = itemView.findViewById(R.id.cardview_myRoutine);
        image = itemView.findViewById(R.id.cardview_myRoutine_img);
        name = itemView.findViewById(R.id.cardview_myRoutine_title);
        desc_short = itemView.findViewById(R.id.cardview_myRoutine_detail);
        numberEx = itemView.findViewById(R.id.cardview_myRoutine_numberEx);
    }

    ImageView getImage() {
        return image;
    }

    public TextView getName() {
        return name;
    }

    TextView getDesc_short() {
        return desc_short;
    }

    TextView getNumberEx() {
        return numberEx;
    }

    CardView getCardView() {
        return cardView;
    }
}
