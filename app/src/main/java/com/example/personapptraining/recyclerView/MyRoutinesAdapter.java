package com.example.personapptraining.recyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.personapptraining.MainActivity;
import com.example.personapptraining.R;
import com.example.personapptraining.fragments.MyRoutineFragment;
import com.example.personapptraining.models.MyRoutine;

import java.util.ArrayList;

public class MyRoutinesAdapter extends RecyclerView.Adapter<MyRoutinesHolder> {
    private FragmentManager fragmentManager;
    private ArrayList<MyRoutine> myRoutines;
    private MyRoutineFragment myRoutineFragment;

    public MyRoutinesAdapter(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
        this.myRoutines = new ArrayList<>();
    }

    public MyRoutineFragment getMyRoutineFragment() {
        return myRoutineFragment;
    }

    @NonNull
    @Override
    public MyRoutinesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_myroutine, parent, false);
        final MyRoutinesHolder myRoutineHolder = new MyRoutinesHolder(view);

        myRoutineHolder.getCardView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity activity = (MainActivity)(fragmentManager.findFragmentByTag("myRoutines-list-by-main")).getActivity();

                myRoutineFragment = new MyRoutineFragment(myRoutines.get(myRoutineHolder.getAdapterPosition()));
                myRoutineFragment.setExercisesAdapter(new ExercisesAdapter(fragmentManager));

                fragmentManager.beginTransaction().replace(R.id.fragment_container, myRoutineFragment,
                        "myRoutine-by-list").commit();

                assert activity != null;
                activity.setCurrentFragment("myRoutine-by-list");

                activity.getSupportActionBar().setTitle(myRoutines.get(myRoutineHolder.getAdapterPosition()).getName());
            }
        });

        return myRoutineHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyRoutinesHolder holder, int position) {
        holder.getName().setText(myRoutines.get(position).getName());
        holder.getDesc_short().setText(myRoutines.get(position).getDesc_short());
        String size = String.valueOf(myRoutines.get(position).getExercises().size());
        holder.getNumberEx().setText(size);
        int resId = holder.itemView.getContext().getResources().getIdentifier("house_exercise",
                "drawable", holder.itemView.getContext().getPackageName());
        Glide.with(holder.itemView.getContext()).load(resId).into(holder.getImage());
    }

    @Override
    public int getItemCount() {
        return myRoutines.size();
    }

    public void addMyRoutines(ArrayList<MyRoutine> myRoutines) {
        this.myRoutines = myRoutines;
        notifyDataSetChanged();
    }
}
