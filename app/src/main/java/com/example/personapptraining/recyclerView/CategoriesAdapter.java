package com.example.personapptraining.recyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.personapptraining.MainActivity;
import com.example.personapptraining.R;
import com.example.personapptraining.fragments.CategoryFragment;
import com.example.personapptraining.models.Category;
import com.example.personapptraining.models.Exercise;

import java.util.ArrayList;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesHolder> {
    private FragmentManager fragmentManager;
    private ArrayList<Category> categories;
    private CategoryFragment categoryFragment;

    public CategoriesAdapter(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
        this.categories = new ArrayList<>();
    }

    public CategoryFragment getCategoryFragment() {
        return categoryFragment;
    }

    @NonNull
    @Override
    public CategoriesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_category, parent, false);
        final CategoriesHolder categoriesHolder = new CategoriesHolder(view);

        categoriesHolder.getCardView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity activity = (MainActivity)(fragmentManager.findFragmentByTag("categories-list-by-main")).getActivity();

                categoryFragment = new CategoryFragment(categories.get(categoriesHolder.getAdapterPosition()));

                assert activity != null;
                ArrayList<Exercise> fullExercises = activity.getExercises();
                ArrayList<Exercise> exercises = new ArrayList<>();
                for(int i = 0;i<fullExercises.size();i++) {
                    if(fullExercises.get(i).getCategory().equals(categoryFragment.getCategory().getName()))   exercises.add(fullExercises.get(i));
                }
                categoryFragment.setExercises(exercises);
                categoryFragment.setExercisesAdapter(new ExercisesAdapter(fragmentManager));

                fragmentManager.beginTransaction().replace(R.id.fragment_container, categoryFragment,
                        "category-by-list").commit();

                activity.setCurrentFragment("category-by-list");

                activity.getSupportActionBar().setTitle(categories.get(categoriesHolder.getAdapterPosition()).getName());
            }
        });

        return categoriesHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CategoriesHolder holder, int position) {
        holder.getName().setText(categories.get(position).getName());
        int resId = holder.itemView.getContext().getResources().getIdentifier(categories.get(position).getId(),
                "drawable", holder.itemView.getContext().getPackageName());
        Glide.with(holder.itemView.getContext()).load(resId).into(holder.getImage());
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public void addCategories(ArrayList<Category> categories) {
        this.categories = categories;
        notifyDataSetChanged();
    }
}
