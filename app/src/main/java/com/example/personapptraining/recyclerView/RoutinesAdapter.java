package com.example.personapptraining.recyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.personapptraining.MainActivity;
import com.example.personapptraining.R;
import com.example.personapptraining.fragments.RoutineFragment;
import com.example.personapptraining.models.Routine;

import java.util.ArrayList;

public class RoutinesAdapter extends RecyclerView.Adapter<RoutinesHolder> {
    private FragmentManager fragmentManager;
    private ArrayList<Routine> routines;
    private RoutineFragment routineFragment;

    public RoutinesAdapter(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
        this.routines = new ArrayList<>();
    }

    public RoutineFragment getRoutineFragment() {
        return routineFragment;
    }

    @NonNull
    @Override
    public RoutinesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_routine, parent, false);
        final RoutinesHolder routinesHolder = new RoutinesHolder(view);

        routinesHolder.getCardView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity activity = (MainActivity)(fragmentManager.findFragmentByTag("routines-list-by-main")).getActivity();

                routineFragment = new RoutineFragment(routines.get(routinesHolder.getAdapterPosition()));
                routineFragment.setExercisesAdapter(new ExercisesAdapter(fragmentManager));

                fragmentManager.beginTransaction().replace(R.id.fragment_container, routineFragment,
                        "routine-by-list").commit();

                assert activity != null;
                activity.setCurrentFragment("routine-by-list");

                activity.getSupportActionBar().setTitle(routines.get(routinesHolder.getAdapterPosition()).getName());
            }
        });

        return routinesHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RoutinesHolder holder, int position) {
        holder.getName().setText(routines.get(position).getName());
        holder.getDesc_short().setText(routines.get(position).getDesc_short());
        String size = String.valueOf(routines.get(position).getExercises().size());
        holder.getNumberEx().setText(size);
        holder.getDifficulty().setText(routines.get(position).getDifficulty());
        int resId = holder.itemView.getContext().getResources().getIdentifier(routines.get(position).getId(),
                "drawable", holder.itemView.getContext().getPackageName());
        Glide.with(holder.itemView.getContext()).load(resId).into(holder.getImage());
    }

    @Override
    public int getItemCount() {
        return routines.size();
    }

    public void addRoutines(ArrayList<Routine> routines) {
        this.routines = routines;
        notifyDataSetChanged();
    }
}
