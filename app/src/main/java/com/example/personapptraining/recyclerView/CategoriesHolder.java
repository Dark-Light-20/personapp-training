package com.example.personapptraining.recyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.personapptraining.R;

public class CategoriesHolder extends RecyclerView.ViewHolder {
    private ImageView image;
    private TextView name;
    private CardView cardView;

    CategoriesHolder(@NonNull View itemView) {
        super(itemView);

        cardView = itemView.findViewById(R.id.cardview_category);
        image = itemView.findViewById(R.id.cardview_category_image);
        name = itemView.findViewById(R.id.cardview_category_name);
    }

    ImageView getImage() {
        return image;
    }

    public TextView getName() {
        return name;
    }

    CardView getCardView() {
        return cardView;
    }
}
