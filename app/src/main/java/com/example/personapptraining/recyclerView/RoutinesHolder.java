package com.example.personapptraining.recyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.personapptraining.R;

public class RoutinesHolder extends RecyclerView.ViewHolder {
    private ImageView image;
    private TextView name, desc_short, numberEx, difficulty;
    private CardView cardView;

    RoutinesHolder(@NonNull View itemView) {
        super(itemView);

        cardView = itemView.findViewById(R.id.cardview_routine);
        image = itemView.findViewById(R.id.cardview_routine_img);
        name = itemView.findViewById(R.id.cardview_routine_title);
        desc_short = itemView.findViewById(R.id.cardview_routine_detail);
        numberEx = itemView.findViewById(R.id.cardview_routine_numberEx);
        difficulty = itemView.findViewById(R.id.cardview_routine_difficulty);
    }

    ImageView getImage() {
        return image;
    }

    public TextView getName() {
        return name;
    }

    TextView getDesc_short() {
        return desc_short;
    }

    TextView getNumberEx() {
        return numberEx;
    }

    TextView getDifficulty() {
        return difficulty;
    }

    CardView getCardView() {
        return cardView;
    }
}
