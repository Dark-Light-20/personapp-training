package com.example.personapptraining.recyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.personapptraining.R;

public class ExercisesHolder extends RecyclerView.ViewHolder {
    private ImageView image;
    private TextView title, category;
    private CardView cardView;

    ExercisesHolder(@NonNull View itemView) {
        super(itemView);

        cardView = itemView.findViewById(R.id.cardview_exercise);
        image = itemView.findViewById(R.id.cardview_exercise_img);
        title = itemView.findViewById(R.id.cardview_exercise_title);
        category = itemView.findViewById(R.id.cardview_exercise_category);
    }

    ImageView getImage() {
        return image;
    }

    TextView getTitle() {
        return title;
    }

    public TextView getCategory() {
        return category;
    }

    CardView getCardView() {
        return cardView;
    }
}
