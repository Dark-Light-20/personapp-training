package com.example.personapptraining.recyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.personapptraining.R;

class AdvicesHolder extends RecyclerView.ViewHolder {
    private ImageView image;
    private TextView name;
    private CardView cardView;

    AdvicesHolder(@NonNull View itemView) {
        super(itemView);

        cardView = itemView.findViewById(R.id.cardview_advice);
        image = itemView.findViewById(R.id.cardview_advice_img);
        name = itemView.findViewById(R.id.cardview_advice_name);
    }

    CardView getCardView() {
        return cardView;
    }

    ImageView getImage() {
        return image;
    }

    TextView getName() {
        return name;
    }
}
