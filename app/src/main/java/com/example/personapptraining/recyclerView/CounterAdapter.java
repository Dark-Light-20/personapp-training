package com.example.personapptraining.recyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.personapptraining.R;
import com.example.personapptraining.models.Record;

import java.util.ArrayList;

public class CounterAdapter extends RecyclerView.Adapter<CounterHolder> {
    private ArrayList<Record> exerciseRecords;

    public CounterAdapter(ArrayList<Record> records) {
        this.exerciseRecords = records;
    }

    @NonNull
    @Override
    public CounterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_exercise, parent, false);
        return new CounterHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CounterHolder holder, int position) {
        holder.getDate().setText(exerciseRecords.get(position).getDate());
        String text = "Realizaste: "+exerciseRecords.get(position).getValue()+" Ejercicios.";
        holder.getText().setText(text);
        Glide.with(holder.itemView.getContext()).load(R.drawable.counter_img).into(holder.getImage());
        holder.getImage().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(),
                        "Tomado de: https://www.flaticon.com/free-icon/jogging_1668531", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return exerciseRecords.size();
    }
}
