package com.example.personapptraining.recyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.personapptraining.R;

public class CounterHolder extends RecyclerView.ViewHolder {
    private ImageView image;
    private TextView date, text;

    CounterHolder(@NonNull View itemView) {
        super(itemView);

        image = itemView.findViewById(R.id.cardview_exercise_img);  // Recycle the Exercise CardView layout
        date = itemView.findViewById(R.id.cardview_exercise_title);
        text = itemView.findViewById(R.id.cardview_exercise_category);
    }

    ImageView getImage() {
        return image;
    }

    TextView getDate() {
        return date;
    }

    public TextView getText() {
        return text;
    }
}
