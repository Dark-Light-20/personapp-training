package com.example.personapptraining.recyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.personapptraining.R;
import com.example.personapptraining.models.Record;

import java.util.ArrayList;

public class WeightAdapter extends RecyclerView.Adapter<CounterHolder> {
    private ArrayList<Record> weightRecords;

    public WeightAdapter(ArrayList<Record> weightRecords) {
        this.weightRecords = weightRecords;
    }

    @NonNull
    @Override
    public CounterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_exercise, parent, false);
        return new CounterHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CounterHolder holder, int position) {
        holder.getDate().setText(weightRecords.get(position).getDate());
        String text = weightRecords.get(position).getValue()+" Kg.";
        holder.getText().setText(text);
        Glide.with(holder.itemView.getContext()).load(R.drawable.weighing_scale_img).into(holder.getImage());
        holder.getImage().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(),
                        "Tomado de: https://www.flaticon.com/free-icon/scale_564970", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.weightRecords.size();
    }

    public void addRecord(Record record) {
        ArrayList<Record> newRecords = new ArrayList<>();
        newRecords.add(record);
        newRecords.addAll(weightRecords);
        weightRecords = newRecords;
        notifyDataSetChanged();
    }
}
