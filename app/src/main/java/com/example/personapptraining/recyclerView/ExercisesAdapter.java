package com.example.personapptraining.recyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.personapptraining.MainActivity;
import com.example.personapptraining.R;
import com.example.personapptraining.fragments.ExerciseFragment;
import com.example.personapptraining.models.Exercise;

import java.util.ArrayList;

public class ExercisesAdapter extends RecyclerView.Adapter<ExercisesHolder> {
    private FragmentManager fragmentManager;
    private ArrayList<Exercise> exercises;
    private ExerciseFragment exercise;

    public ExercisesAdapter(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
        exercises = new ArrayList<>();
    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    @NonNull
    @Override
    public ExercisesHolder onCreateViewHolder(@NonNull final ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_exercise, parent, false);
        final ExercisesHolder exercisesHolder = new ExercisesHolder(view);

        exercisesHolder.getCardView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exercise = new ExerciseFragment(exercises.get(exercisesHolder.getAdapterPosition()));

                MainActivity activity;
                Fragment fragment = fragmentManager.findFragmentByTag("exercise-list-by-main");
                if (fragment == null) {
                    fragment = fragmentManager.findFragmentByTag("category-by-list");
                    if (fragment == null) {
                        fragment = fragmentManager.findFragmentByTag("routine-by-list");
                        if (fragment == null) {
                            fragment = fragmentManager.findFragmentByTag("myRoutine-by-list");
                            assert fragment != null;
                            activity = (MainActivity)fragment.getActivity();
                            fragmentManager.beginTransaction().replace(R.id.fragment_container, exercise,
                                    "exercise-by-myRoutine").commit();
                            assert activity != null;
                            activity.setCurrentFragment("exercise-by-myRoutine");
                        }
                        else {
                            activity = (MainActivity)fragment.getActivity();
                            fragmentManager.beginTransaction().replace(R.id.fragment_container, exercise,
                                    "exercise-by-routine").commit();
                            assert activity != null;
                            activity.setCurrentFragment("exercise-by-routine");
                        }
                    }
                    else {
                        activity = (MainActivity)fragment.getActivity();
                        fragmentManager.beginTransaction().replace(R.id.fragment_container, exercise,
                                "exercise-by-category").commit();
                        assert activity != null;
                        activity.setCurrentFragment("exercise-by-category");
                    }
                }
                else {
                    activity = (MainActivity)fragment.getActivity();
                    fragmentManager.beginTransaction().replace(R.id.fragment_container, exercise,
                            "exercise-by-list").commit();
                    assert activity != null;
                    activity.setCurrentFragment("exercise-by-list");
                }

                activity.getSupportActionBar().setTitle(exercises.get(exercisesHolder.getAdapterPosition()).getName());
            }
        });

        return exercisesHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ExercisesHolder holder, int position) {
        holder.getTitle().setText(exercises.get(position).getName());
        holder.getCategory().setText(exercises.get(position).getCategory());
        int resId = holder.itemView.getContext().getResources().getIdentifier(exercises.get(position).getId(),
                "drawable", holder.itemView.getContext().getPackageName());
        Glide.with(holder.itemView.getContext()).load(resId).into(holder.getImage());
    }

    @Override
    public int getItemCount() {
        return exercises.size();
    }

    public void addExercises(ArrayList<Exercise> exercises) {
        this.exercises = exercises;
        notifyDataSetChanged();
    }
}
