package com.example.personapptraining.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.personapptraining.models.Advice;
import com.example.personapptraining.models.Category;
import com.example.personapptraining.models.Exercise;
import com.example.personapptraining.models.MyRoutine;
import com.example.personapptraining.models.Record;
import com.example.personapptraining.models.Routine;

import java.util.ArrayList;
import java.util.Arrays;

public class DataBaseAccess {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase db;
    private static DataBaseAccess instance;
    private Cursor c = null;

    private DataBaseAccess(Context context) {
        this.openHelper = new DataBaseOpenHelper(context);
    }

    public static DataBaseAccess getInstance(Context context) {
        if (instance==null) {
            instance = new DataBaseAccess(context);
        }
        return instance;
    }

    public void open() {
        this.db = openHelper.getWritableDatabase();
    }

    public void close() {
        if (db!=null)    this.db.close();
    }

    public ArrayList<Exercise> getDbExercises() {
        c = db.rawQuery("select * from ejercicios", new String[]{});

        ArrayList<Exercise> exercises = new ArrayList<>();

        ArrayList<String> steps;

        if (c.moveToFirst()) {
            while (!c.isAfterLast()) {
                String id = c.getString(c.getColumnIndex("cod_ejer"));
                String name = c.getString(c.getColumnIndex("nombre"));
                String categoryID = c.getString(c.getColumnIndex("categoria"));
                String category = "";
                Cursor c2 = db.rawQuery("select nombre from categorias where cod_cat=='"+categoryID+"'", null);
                if (c2.moveToFirst()) {
                    category = c2.getString(0);
                }
                c2.close();
                String series = c.getString(c.getColumnIndex("series"));
                String repetitions = c.getString(c.getColumnIndex("repeticiones"));
                String time = c.getString(c.getColumnIndex("tiempo"));
                String fullSteps = c.getString(c.getColumnIndex("pasos"));
                steps = new ArrayList<>(Arrays.asList(fullSteps.split(",")));

                Exercise exercise = new Exercise(id, name, series, time, repetitions, category, steps);
                exercises.add(exercise);
                c.moveToNext();
            }
        }
        return exercises;
    }

    public ArrayList<Category> getDbCategories() {
        c = db.rawQuery("select * from categorias", new String[]{});

        ArrayList<Category> categories = new ArrayList<>();

        if (c.moveToFirst()) {
            while (!c.isAfterLast()) {
                String id = c.getString(c.getColumnIndex("cod_cat"));
                String name = c.getString(c.getColumnIndex("nombre"));
                String desc = c.getString(c.getColumnIndex("detalles"));

                Category category = new Category(id, name, desc);
                categories.add(category);
                c.moveToNext();
            }
        }
        return categories;
    }

    public ArrayList<Routine> getDbRoutines(ArrayList<Exercise> exercises) {
        c = db.rawQuery("select * from rutinas", new String[]{});

        ArrayList<Routine> routines = new ArrayList<>();

        if (c.moveToFirst()) {
            while (!c.isAfterLast()) {
                String id = c.getString(c.getColumnIndex("cod_rut"));
                String cod_rut = c.getString(c.getColumnIndex("cod_rut"));
                String name = c.getString(c.getColumnIndex("nombre"));
                String desc = c.getString(c.getColumnIndex("detalle"));
                String desc_short = c.getString(c.getColumnIndex("detalle_corto"));
                String difficulty = c.getString(c.getColumnIndex("dificultad"));

                Cursor c2 = db.rawQuery("select ejercicios.nombre from ejercicios join rutina_x_ejercicio " +
                        "where ejercicios.cod_ejer = rutina_x_ejercicio.cod_ejer and rutina_x_ejercicio.cod_rut = '"
                        + cod_rut + "'", null);
                c2.moveToFirst();
                ArrayList<Exercise> routineExercises = new ArrayList<>();

                while (!c2.isAfterLast()) {
                    for(int i=0;i<exercises.size();i++) {
                        if (exercises.get(i).getName().equals(c2.getString(0))) {
                            routineExercises.add(exercises.get(i));
                            break;
                        }
                    }
                    c2.moveToNext();
                }

                c2.close();
                Routine routine = new Routine(id, name, difficulty, desc, desc_short, routineExercises);
                routines.add(routine);
                c.moveToNext();
            }
        }
        return routines;
    }

    public ArrayList<MyRoutine> getDbMyRoutines(ArrayList<Exercise> exercises) {
        c = db.rawQuery("select * from mis_rutinas", new String[]{});

        ArrayList<MyRoutine> myRoutines = new ArrayList<>();

        if (c.moveToFirst()) {
            while (!c.isAfterLast()) {
                String cod_my_rut = c.getString(c.getColumnIndex("cod_my_rut"));
                String name = c.getString(c.getColumnIndex("nombre"));
                String desc = c.getString(c.getColumnIndex("detalle"));
                String desc_short = c.getString(c.getColumnIndex("detalle_corto"));

                Cursor c2 = db.rawQuery("select ejercicios.nombre from ejercicios join mi_rutina_x_ejercicio " +
                        "where ejercicios.cod_ejer = mi_rutina_x_ejercicio.cod_ejer and mi_rutina_x_ejercicio.cod_my_rut = '"
                        + cod_my_rut + "'", null);
                c2.moveToFirst();
                ArrayList<Exercise> routineExercises = new ArrayList<>();

                while (!c2.isAfterLast()) {
                    for(int i=0;i<exercises.size();i++) {
                        if (exercises.get(i).getName().equals(c2.getString(c2.getColumnIndex("nombre")))) {
                            routineExercises.add(exercises.get(i));
                            break;
                        }
                    }
                    c2.moveToNext();
                }

                c2.close();
                MyRoutine myRoutine = new MyRoutine(name, desc, desc_short, routineExercises);
                myRoutines.add(myRoutine);
                c.moveToNext();
            }
        }
        return myRoutines;
    }

    public void getDbExRecords(ArrayList<Record> exerciseRecords) {
        c = db.rawQuery("select * from registro_ejercicios", new String[]{});

        if (c.moveToLast()) {
            while (!c.isBeforeFirst()) {
                String date = c.getString(c.getColumnIndex("fecha"));
                String value = c.getString(c.getColumnIndex("numero"));
                exerciseRecords.add(new Record(date, value));
                c.moveToPrevious();
            }
        }
    }

    public void addDbExRecord(Record record) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("fecha", record.getDate());
        contentValues.put("numero", record.getValue());
        this.db.insert("registro_ejercicios", null, contentValues);
    }

    public void addToCurrentExRecord() {
        c = db.rawQuery("select * from registro_ejercicios", new String[]{});

        if (c.moveToLast()) {
            if (!c.isBeforeFirst()) {
                String date = c.getString(c.getColumnIndex("fecha"));
                String value = c.getString(c.getColumnIndex("numero"));
                int number = Integer.parseInt(value);
                number++;
                ContentValues contentValues = new ContentValues();
                contentValues.put("fecha", date);
                contentValues.put("numero", number);
                this.db.update("registro_ejercicios", contentValues,"fecha='"+date+"'", null);
            }
        }
    }

    public void getDbWgRecords(ArrayList<Record> weightRecords) {
        c = db.rawQuery("select * from registro_peso", new String[]{});

        if (c.moveToLast()) {
            while (!c.isBeforeFirst()) {
                String date = c.getString(c.getColumnIndex("fecha"));
                String value = c.getString(c.getColumnIndex("peso"));
                weightRecords.add(new Record(date, value));
                c.moveToPrevious();
            }
        }
    }

    public String getLastIdealWgRecord() {
        c = db.rawQuery("select * from registro_peso", new String[]{});

        if (c.moveToLast()) {
            if (!c.isBeforeFirst()) {
                return c.getString(c.getColumnIndex("ideal"));
            }
        }
        return "0";
    }

    public void addDbWgRecord(Record record, String ideal) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("fecha", record.getDate());
        contentValues.put("peso", record.getValue());
        contentValues.put("ideal", ideal);
        this.db.insert("registro_peso", null, contentValues);

        ArrayList<Record> trys = new ArrayList<>();
        getDbWgRecords(trys);
    }

    public void insertToMyRoutines(MyRoutine myRoutine, int actualSize) {
        String cod_my_rut = String.valueOf(actualSize+1);
        String name = myRoutine.getName();
        String desc = myRoutine.getDesc();
        String desc_short = myRoutine.getDesc_short();

        ContentValues myRoutineRegistry = new ContentValues();
        myRoutineRegistry.put("cod_my_rut", cod_my_rut);
        myRoutineRegistry.put("nombre", name);
        myRoutineRegistry.put("detalle", desc);
        myRoutineRegistry.put("detalle_corto", desc_short);

        this.db.insert("mis_rutinas", null, myRoutineRegistry);

        for(int i=0;i<myRoutine.getExercises().size();i++) {
            c = db.rawQuery("select * from ejercicios", new String[]{});

            if (c.moveToFirst()) {
                while (!c.isAfterLast()) {
                    String nombre = c.getString(c.getColumnIndex("nombre"));
                    if (nombre.equals(myRoutine.getExercises().get(i).getName())) {
                        String cod_ejer = c.getString(c.getColumnIndex("cod_ejer"));
                        ContentValues myRoutinexExerciseRegistry = new ContentValues();
                        myRoutinexExerciseRegistry.put("cod_my_rut", cod_my_rut);
                        myRoutinexExerciseRegistry.put("cod_ejer", cod_ejer);

                        this.db.insert("mi_rutina_x_ejercicio", null, myRoutinexExerciseRegistry);
                        break;
                    }
                    c.moveToNext();
                }
            }
        }
    }

    public void getDbAdvices(ArrayList<Advice> advices) {
        c = db.rawQuery("select * from consejos", new String[]{});

        if (c.moveToFirst()) {
            while (!c.isAfterLast()) {
                String name = c.getString(c.getColumnIndex("nombre"));
                String text = c.getString(c.getColumnIndex("texto"));
                advices.add(new Advice(name, text));
                c.moveToNext();
            }
        }
    }
}
