package com.example.personapptraining.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

class DataBaseOpenHelper extends SQLiteAssetHelper {
    private static final String DATABASE_NAME="personAPP-DB.db";
    private static int DATABASE_VERSION=1;

    DataBaseOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        DATABASE_VERSION = newVersion;
        super.onUpgrade(db, oldVersion, newVersion);
    }
}
