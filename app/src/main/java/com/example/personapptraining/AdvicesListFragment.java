package com.example.personapptraining;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.personapptraining.recyclerView.AdvicesAdapter;

public class AdvicesListFragment extends Fragment {
    AdvicesAdapter advicesAdapter;

    AdvicesListFragment(FragmentManager fragmentManager) {
        this.advicesAdapter = new AdvicesAdapter(fragmentManager);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_layout_exercise_list, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.exercises_recycler_view);
        recyclerView.setAdapter(advicesAdapter);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);

        return view;
    }
}
