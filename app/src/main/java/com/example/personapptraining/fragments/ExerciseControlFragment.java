package com.example.personapptraining.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.personapptraining.MainActivity;
import com.example.personapptraining.R;
import com.example.personapptraining.models.Record;
import com.example.personapptraining.recyclerView.CounterAdapter;

import java.util.ArrayList;

public class ExerciseControlFragment extends Fragment {
    private ArrayList<Record> exerciseRecords;

    public ExerciseControlFragment(ArrayList<Record> exerciseRecords) {
        this.exerciseRecords = exerciseRecords;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_layout_exercises_control, container, false);

        TextView counter = view.findViewById(R.id.counter_num);
        counter.setText(((MainActivity)getActivity()).getCurrentExRecord().getValue());

        RecyclerView recyclerView = view.findViewById(R.id.counter_registry_recycler_view);
        recyclerView.setAdapter(new CounterAdapter(exerciseRecords));
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);

        return  view;
    }
}
