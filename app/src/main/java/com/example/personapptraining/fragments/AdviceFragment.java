package com.example.personapptraining.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.personapptraining.R;
import com.example.personapptraining.models.Advice;

public class AdviceFragment extends Fragment {
    private Advice advice;

    public AdviceFragment(Advice advice) {
        this.advice = advice;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_layout_advice, container, false);

        TextView name = view.findViewById(R.id.advice_name);
        TextView text = view.findViewById(R.id.advice_text);

        name.setText(advice.getName());
        text.setText(advice.getText());

        view.findViewById(R.id.advice_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Tomado de: https://www.flaticon.com/free-icon/exercise_2309919",
                        Toast.LENGTH_LONG).show();
            }
        });

        return view;
    }
}
