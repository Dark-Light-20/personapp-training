package com.example.personapptraining.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.personapptraining.MainActivity;
import com.example.personapptraining.R;
import com.example.personapptraining.models.Category;
import com.example.personapptraining.models.Exercise;
import com.example.personapptraining.recyclerView.ExercisesAdapter;

import java.util.ArrayList;

public class CategoryFragment extends Fragment {
    private Category category;
    private ArrayList<Exercise> exercises;
    private ExercisesAdapter exercisesAdapter;

    public CategoryFragment(Category category) {
        this.category = category;
        this.exercises = new ArrayList<>();
    }

    public Category getCategory() {
        return category;
    }

    public void setExercisesAdapter(ExercisesAdapter exercisesAdapter) {
        this.exercisesAdapter = exercisesAdapter;
    }

    public void setExercises(ArrayList<Exercise> exercises) {
        this.exercises = exercises;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_layout_category, container, false);

        ImageView imageView = view.findViewById(R.id.category_image);
        TextView name = view.findViewById(R.id.category_name);
        TextView desc = view.findViewById(R.id.category_detail);

        name.setText(category.getName());
        desc.setText(category.getDesc());

        int resId = getContext().getResources().getIdentifier(category.getId(), "drawable", getContext().getPackageName());
        Glide.with(getContext()).load(resId).into(imageView);

        final String source = ((MainActivity)getActivity()).getImageSource(category.getId());
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), source, Toast.LENGTH_LONG).show();
            }
        });

        RecyclerView recyclerView = view.findViewById(R.id.exercise_list_by_category);
        recyclerView.setAdapter(exercisesAdapter);
        exercisesAdapter.addExercises(exercises);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);

        return view;
    }
}
