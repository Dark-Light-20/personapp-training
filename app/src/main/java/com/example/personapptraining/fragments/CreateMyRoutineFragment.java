package com.example.personapptraining.fragments;

import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.personapptraining.MainActivity;
import com.example.personapptraining.R;
import com.example.personapptraining.models.Exercise;
import com.example.personapptraining.models.MyRoutine;

import java.util.ArrayList;

public class CreateMyRoutineFragment extends Fragment {
    private TextView name, desc, desc_short;
    private ArrayList<Exercise> exercises;
    private ListView listView;

    public CreateMyRoutineFragment() {
        this.exercises = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_layout_createmyroutine, container, false);

        name = view.findViewById(R.id.new_name);
        desc = view.findViewById(R.id.new_desc);
        desc_short = view.findViewById(R.id.new_desc_short);
        listView = view.findViewById(R.id.exercises_names);
        Button button = view.findViewById(R.id.btn_create_myRoutine);

        ArrayList<Exercise> fullExercises = ((MainActivity)getActivity()).getExercises();
        ArrayList<String> exercisesNames = new ArrayList<>();
        for (int i=0;i<fullExercises.size();i++)    exercisesNames.add(fullExercises.get(i).getName());
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_list_item_multiple_choice, exercisesNames);
        listView.setAdapter(arrayAdapter);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity activity = ((MainActivity)getActivity());
                SparseBooleanArray checkedItems = listView.getCheckedItemPositions();

                String nameString = name.getText().toString();
                String descString = desc.getText().toString();
                String desc_shortString = desc_short.getText().toString();
                if (nameString.isEmpty() || descString.isEmpty() || desc_shortString.isEmpty()) {
                    Toast.makeText(getContext(), "Campos invalidos, verifica que no estén vacíos...",
                            Toast.LENGTH_LONG).show();
                }
                else {
                    for (int i=0;i<checkedItems.size();i++) {
                        if (checkedItems.valueAt(i)) {
                            assert activity != null;
                            Exercise addExercise = activity.getExerciseByName(listView.getAdapter().getItem(checkedItems.keyAt(i)).toString());
                            if (addExercise!=null)  exercises.add(addExercise);
                        }
                    }
                    if (exercises.size() == 0) {
                        Toast.makeText(getContext(), "No has seleccionado ningún ejercicio", Toast.LENGTH_LONG).show();
                        return;
                    }
                    MyRoutine myRoutine = new MyRoutine(name.getText().toString(), desc.getText().toString(), desc_short.getText().toString(), exercises);
                    assert activity != null;
                    activity.addToMyRoutines(myRoutine);
                    Toast.makeText(getContext(), "Rutina añadida", Toast.LENGTH_LONG).show();
                    activity.onBackPressed();
                    activity.setCurrentFragment("myRoutines-list-by-main");
                    activity.getSupportActionBar().setTitle(R.string.my_routines);
                }
            }
        });

        return view;
    }
}
