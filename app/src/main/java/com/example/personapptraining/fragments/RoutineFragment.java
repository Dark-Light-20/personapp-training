package com.example.personapptraining.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.personapptraining.MainActivity;
import com.example.personapptraining.R;
import com.example.personapptraining.models.Routine;
import com.example.personapptraining.recyclerView.ExercisesAdapter;

public class RoutineFragment extends Fragment {
    private Routine routine;
    private int next_exercise = 0;
    private ExercisesAdapter exercisesAdapter;

    public RoutineFragment(Routine routine) {
        this.routine = routine;
    }

    public Routine getRoutine() {
        return routine;
    }

    public void setExercisesAdapter(ExercisesAdapter exercisesAdapter) {
        this.exercisesAdapter = exercisesAdapter;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_layout_routine, container, false);

        ImageView imageView = view.findViewById(R.id.routine_image);
        TextView name = view.findViewById(R.id.routine_name);
        TextView desc = view.findViewById(R.id.routine_detail);
        TextView numberEx = view.findViewById(R.id.routine_numberEx);
        TextView difficulty = view.findViewById(R.id.routine_difficulty);
        Button button = view.findViewById(R.id.btn_start);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next_exercise = 0;
                ExerciseFragment exerciseFragment = new ExerciseFragment(routine.getExercises(), next_exercise, false);
                assert getFragmentManager() != null;
                getFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        exerciseFragment, "exercise-by-routine").commit();
                ((MainActivity)getActivity()).setCurrentFragment("exercise-by-routine");
            }
        });

        name.setText(routine.getName());
        desc.setText(routine.getDesc());
        String size = String.valueOf(routine.getExercises().size());
        numberEx.setText(size);
        difficulty.setText(routine.getDifficulty());

        int resId = getContext().getResources().getIdentifier(routine.getId(), "drawable", getContext().getPackageName());
        Glide.with(getContext()).load(resId).into(imageView);

        final String source = ((MainActivity)getActivity()).getImageSource(routine.getId());
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), source, Toast.LENGTH_LONG).show();
            }
        });

        RecyclerView recyclerView = view.findViewById(R.id.exercise_list_by_routine);
        recyclerView.setAdapter(exercisesAdapter);
        exercisesAdapter.addExercises(routine.getExercises());
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);

        return view;
    }
}
