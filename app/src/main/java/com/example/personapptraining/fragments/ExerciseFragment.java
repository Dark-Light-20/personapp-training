package com.example.personapptraining.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.example.personapptraining.MainActivity;
import com.example.personapptraining.R;
import com.example.personapptraining.models.Exercise;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

public class ExerciseFragment extends Fragment {
    private Exercise exercise;
    private ArrayList<Exercise> exercisesRoutine;
    private int nextExercise;
    private boolean myRoutine;

    public ExerciseFragment(Exercise exercise) {
        this.exercise = exercise;
    }

    ExerciseFragment(ArrayList<Exercise> exercises, int nextExercise, boolean myRoutine) {
        this.exercisesRoutine = exercises;
        this.nextExercise = nextExercise;
        this.exercise = exercises.get(nextExercise);
        this.myRoutine = myRoutine;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_layout_exercise, container, false);

        ImageView imageView = view.findViewById(R.id.exercise_image);
        TextView name = view.findViewById(R.id.exercise_name);
        TextView category = view.findViewById(R.id.exercise_category);
        TextView series = view.findViewById(R.id.exercise_series);
        TextView repetitions = view.findViewById(R.id.exercise_repetitions);
        TextView time = view.findViewById(R.id.exercise_time);
        TextView steps = view.findViewById(R.id.exercise_steps);
        FloatingActionButton btnNextExercise = view.findViewById(R.id.btn_next_exercise);

        if (exercisesRoutine != null && nextExercise != exercisesRoutine.size()-1) {
            btnNextExercise.setVisibility(View.VISIBLE);
            btnNextExercise.setEnabled(true);
            btnNextExercise.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ExerciseFragment exerciseFragment = new ExerciseFragment(exercisesRoutine, nextExercise+1, myRoutine);
                    assert getFragmentManager() != null;
                    if (myRoutine) {
                        getFragmentManager().beginTransaction().replace(R.id.fragment_container,
                                exerciseFragment, "exercise-by-myRoutine").commit();
                        ((MainActivity)getActivity()).setCurrentFragment("exercise-by-myRoutine");
                    }
                    else {
                        getFragmentManager().beginTransaction().replace(R.id.fragment_container,
                                exerciseFragment, "exercise-by-routine").commit();
                        ((MainActivity)getActivity()).setCurrentFragment("exercise-by-routine");
                    }
                }
            });
        }

        name.setText(exercise.getName());
        category.setText(exercise.getCategory());
        series.setText(exercise.getSeries());
        repetitions.setText(exercise.getRepetitions());
        time.setText(exercise.getTime());

        StringBuilder stepsString = new StringBuilder();
        ArrayList<String> exerciseSteps = exercise.getSteps();
        for(int i=0;i<exerciseSteps.size();i++) {
            stepsString.append(i + 1).append(". ").append(exerciseSteps.get(i)).append("\n");
        }

        steps.setText(stepsString);

        int resId = getContext().getResources().getIdentifier(exercise.getId(), "drawable", getContext().getPackageName());
        Glide.with(getContext()).load(resId).into(imageView);

        final String source = ((MainActivity)getActivity()).getImageSource(exercise.getId());
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), source, Toast.LENGTH_LONG).show();
            }
        });

        ((MainActivity) getActivity()).addCurrentEx();

        return view;
    }
}
