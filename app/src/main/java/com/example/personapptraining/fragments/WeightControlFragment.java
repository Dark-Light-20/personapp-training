package com.example.personapptraining.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.personapptraining.MainActivity;
import com.example.personapptraining.R;
import com.example.personapptraining.models.Record;
import com.example.personapptraining.recyclerView.WeightAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

public class WeightControlFragment extends Fragment {
    private ArrayList<Record> weightRecords;
    private WeightAdapter weightAdapter;
    private TextView remainWeight;

    public WeightControlFragment(ArrayList<Record> weightRecords) {
        this.weightRecords = weightRecords;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_layout_weight_control, container, false);

        final TextView actualWg = view.findViewById(R.id.actual_weight_num);
        final TextView idealWg = view.findViewById(R.id.ideal_weight_num);
        remainWeight = view.findViewById(R.id.remain_weight);

        final String weight, ideal;
        Record actualRecord = ((MainActivity)getActivity()).getCurrentWgRecord();
        if (actualRecord!=null) weight = ((MainActivity)getActivity()).getCurrentWgRecord().getValue()+" Kg.";
        else    weight = "0 Kg.";
        ideal = ((MainActivity)getActivity()).getIdealWeight()+" Kg.";
        actualWg.setText(weight);
        idealWg.setText(ideal);

        String tempWg = weight;
        tempWg = tempWg.substring(0, tempWg.length()-3).replaceAll("\\s","");
        String tempIdeal = ideal;
        tempIdeal = tempIdeal.substring(0, tempIdeal.length()-3).replaceAll("\\s","");
        updateRemain(Integer.parseInt(tempWg), Integer.parseInt(tempIdeal));

        RecyclerView recyclerView = view.findViewById(R.id.weight_registry_recycler_view);
        weightAdapter = new WeightAdapter(weightRecords);
        recyclerView.setAdapter(weightAdapter);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);

        FloatingActionButton btn_update_weight = view.findViewById(R.id.btn_update_weight);
        FloatingActionButton btn_update_ideal = view.findViewById(R.id.btn_update_ideal);

        btn_update_weight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!idealWg.getText().equals("0 Kg.")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Ingresa tu peso actual");

                    final EditText input = new EditText(getContext());
                    input.setInputType(InputType.TYPE_CLASS_NUMBER);
                    builder.setView(input);

                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String text = input.getText().toString()+" Kg.";
                            actualWg.setText(text);
                            Record newRecord = new Record(DateFormat.getDateInstance().format(new Date()),
                                    input.getText().toString());
                            weightAdapter.addRecord(newRecord);
                            String weight = actualWg.getText().toString();
                            weight = weight.substring(0, weight.length()-3).replaceAll("\\s","");
                            String ideal = idealWg.getText().toString();
                            ideal = ideal.substring(0, ideal.length()-3).replaceAll("\\s","");
                            ((MainActivity)getActivity()).addWeightRecord(newRecord, ideal);
                            updateRemain(Integer.parseInt(weight), Integer.parseInt(ideal));
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    builder.show();
                }
                else    Toast.makeText(getContext(), "Primero ingresa tu peso ideal", Toast.LENGTH_LONG).show();
            }
        });

        btn_update_ideal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Ingresa tu peso ideal");

                final EditText input = new EditText(getContext());
                input.setInputType(InputType.TYPE_CLASS_NUMBER);
                builder.setView(input);

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String text = input.getText().toString()+" Kg.";
                        idealWg.setText(text);
                        ((MainActivity)getActivity()).setIdealWeight(input.getText().toString());
                        String weight = actualWg.getText().toString();
                        weight = weight.substring(0, weight.length()-3).replaceAll("\\s","");
                        String ideal = idealWg.getText().toString();
                        ideal = ideal.substring(0, ideal.length()-3).replaceAll("\\s","");
                        updateRemain(Integer.parseInt(weight), Integer.parseInt(ideal));
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        });

        return  view;
    }

    private void updateRemain(int weight, int ideal) {
        String text;
        remainWeight.setTypeface(Typeface.DEFAULT);
        if (weight<ideal)   text = "Te falta aumentar "+(ideal-weight)+" Kg.";
        else if (weight>ideal)  text = "Te falta rebajar "+(weight-ideal)+" Kg.";
        else {
            text = "Llegaste a tu meta!!!";
            remainWeight.setTypeface(Typeface.DEFAULT_BOLD);
        }
        remainWeight.setText(text);
    }
}
