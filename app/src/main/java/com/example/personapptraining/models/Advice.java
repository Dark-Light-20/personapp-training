package com.example.personapptraining.models;

public class Advice {
    private String name, text;

    public Advice(String name, String text) {
        this.name = name;
        this.text = text;
    }

    public String getName() {
        return name;
    }

    public String getText() {
        return text;
    }
}
