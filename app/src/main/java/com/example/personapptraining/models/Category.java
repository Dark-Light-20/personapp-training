package com.example.personapptraining.models;

public class Category {
    private String id, name, desc;

    public Category(String id, String name, String desc) {
        this.id = "c"+id;
        this.name = name;
        this.desc = desc;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }
}
