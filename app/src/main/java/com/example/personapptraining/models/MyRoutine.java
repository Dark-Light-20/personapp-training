package com.example.personapptraining.models;

import java.util.ArrayList;

public class MyRoutine {
    private String name, desc, desc_short;
    private ArrayList<Exercise> exercises;

    public MyRoutine(String name, String desc, String desc_short, ArrayList<Exercise> exercises) {
        this.name = name;
        this.desc = desc;
        this.desc_short = desc_short;
        this.exercises = exercises;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public String getDesc_short() {
        return desc_short;
    }

    public ArrayList<Exercise> getExercises() {
        return exercises;
    }
}
