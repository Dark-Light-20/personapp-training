package com.example.personapptraining.models;

import java.util.ArrayList;

public class Routine {
    private String id, name, difficulty, desc, desc_short;
    private ArrayList<Exercise> exercises;

    public Routine(String id, String name, String difficulty, String desc, String desc_short, ArrayList<Exercise> exercises) {
        this.id = "r"+id;
        this.name = name;
        switch (difficulty) {
            case "1":
                this.difficulty = "Principiante";
                break;
            case "2":
                this.difficulty = "Intermedio";
                break;
            case "3":
                this.difficulty = "Dificil";
                break;
        }
        this.desc = desc;
        this.desc_short = desc_short;
        this.exercises = exercises;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public String getDesc() {
        return desc;
    }

    public String getDesc_short() {
        return desc_short;
    }

    public ArrayList<Exercise> getExercises() {
        return exercises;
    }
}
