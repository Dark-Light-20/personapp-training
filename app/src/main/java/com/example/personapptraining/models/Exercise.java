package com.example.personapptraining.models;

import java.util.ArrayList;

public class Exercise {
    private String id, name, series, time, repetitions, category;
    private ArrayList<String> steps;

    public Exercise(String id, String name, String series, String time, String repetitions, String category, ArrayList<String> steps) {
        this.id = "e"+id;
        this.name = name;
        this.series = series;
        this.time = time;
        this.repetitions = repetitions;
        this.category = category;
        this.steps = steps;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSeries() {
        return series;
    }

    public String getTime() {
        return time;
    }

    public String getRepetitions() {
        return repetitions;
    }

    public String getCategory() {
        return category;
    }

    public ArrayList<String> getSteps() {
        return steps;
    }
}
